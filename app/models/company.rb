class Company < ActiveRecord::Base
  validates :name, :manager, :status, :terms, presence: true

  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      Company.create! row.to_hash
    end
  end
end
