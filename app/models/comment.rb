class Comment < ActiveRecord::Base
  belongs_to :article
  belongs_to :user

  validates :body, presence: true
  
  default_scope { order(created_at: :desc) }

  def self.persisted
    where.not(id: nil)
  end
end
