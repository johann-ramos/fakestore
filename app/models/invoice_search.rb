class InvoiceSearch
  attr_reader  :date_from, :date_to

  def initialize(params)
    params ||= {}
    @date_from = parsed_data(params[:date_from], 30.days.ago.to_date.to_s)
    @date_to = parsed_data(params[:date_to], Date.today.to_s)
  end

  def scope
    Invoice.where('created_at BETWEEN ? AND ?', @date_from, @date_to)
  end

  private

  def parsed_data(date_string,default)
    Date.parse(date_string)
  rescue ArgumentError, TypeError
    default
  end
end

%w{start stop restart}.each do |i|
  puts "#{i}"
end