class Invoice < ActiveRecord::Base
  validates :company, :tax, :salesperson, presence: true
  validates :tax, numericality: { greater_than: 0 }
end