# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

[User].each(&:delete_all)

useradmin = User.new.tap do |u|
  u.email = 'johann.ramos.r@gmail.com'
  u.password = 'Cho4uig4'
  u.password_confirmation = 'Cho4uig4'
  u.name = 'Johann Ramos'
  u.skip_confirmation!
  u.save!
end

usertest1 = User.new.tap do |u|
  u.email = 'johann.ramos.r@outlook.com'
  u.password = 'Cho4uig4'
  u.password_confirmation = 'Cho4uig4'
  u.name = 'Johann Stark'
  u.skip_confirmation!
  u.save!
end

usertest2 = User.new.tap do |u|
  u.email = 'johann.ramos@zpricing.com'
  u.password = 'Cho4uig4'
  u.password_confirmation = 'Cho4uig4'
  u.name = 'John Snow'
  u.skip_confirmation!
  u.save!
end