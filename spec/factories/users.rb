FactoryGirl.define do
  factory :user do
    name  Faker::Name.name
    email Faker::Internet.email
    password Faker::Internet.password
    confirmed_at Time.now
    confirmation_sent_at Time.now
  end
end