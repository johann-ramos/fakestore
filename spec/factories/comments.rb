FactoryGirl.define do
  factory :comment do
    body Faker::Lorem.paragraph(2)
    created_at Time.now
    updated_at Time.now
  end
end