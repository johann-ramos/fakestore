FactoryGirl.define do
  factory :article do
    title  Faker::Lorem.sentence
    body Faker::Lorem.paragraph(2)
    created_at Time.now
    updated_at Time.now
  end
end