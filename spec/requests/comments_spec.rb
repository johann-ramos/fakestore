require 'rails_helper'

RSpec.describe "Comments", type: :request do
  before do
    @article = FactoryGirl.create(:article)
  end
  describe "GET /comments" do
    it "works! (now write some real specs)" do

      get article_comments_path(@article)
      expect(response).to have_http_status(200)
    end
  end
end
