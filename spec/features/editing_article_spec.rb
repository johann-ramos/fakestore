require 'rails_helper'

RSpec.feature "Editing Article" do
  let(:user) { FactoryGirl.create(:user)}
  before do
    login_as(user)
    @article = Article.create(title: "The first article", body: "Body of first article", user: user)
  end

  scenario "A user updates an article" do
    visit "/articles"
    click_link @article.title
    click_link "Edit Article"

    fill_in "Title", with: "Updated title article"
    fill_in "Body", with: "Updated body article"
    click_button "Update Article"

    expect(page).to have_content("Article was successfully updated")
    expect(page.current_path).to eq(article_path(@article))
  end

  scenario "A user fails to update an article" do
    visit "/articles"
    click_link @article.title
    click_link "Edit Article"

    fill_in "Title", with: ""
    fill_in "Body", with: "Updated body article"
    click_button "Update Article"

    expect(page).to have_content("Title can't be blank")
    expect(page.current_path).to eq(article_path(@article))
  end
end