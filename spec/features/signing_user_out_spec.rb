require 'rails_helper'

RSpec.feature "Signing out signed-in users" do
  let(:user) { FactoryGirl.create(:user)}
  before { login }

  def login
    visit '/'
    click_link 'Log In'
    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_button 'Login'
  end

  scenario "Logout" do
    click_link "Log Out"
    expect(page).to have_content("Signed out successfully.")
  end

end