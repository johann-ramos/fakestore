require 'rails_helper'

RSpec.feature "Listing Articles" do
  let(:user) { FactoryGirl.create(:user)}
  before do
    @article1 = Article.create(title: "The first article", body: "Body of first article", user: user)
    @article2 = Article.create(title: "The second article", body: "Body of second article", user: user)
  end

  scenario "List all articles" do
    visit "/articles"

    [@article1, @article2].each do |article|
      expect(page).to have_content(article.title)
      expect(page).to have_content(article.body)
      expect(page).to have_link(article.title)
    end
    expect(page).not_to have_link("New Article")
  end
end