require 'rails_helper'

RSpec.feature "Showing Article" do
  let(:user1) { FactoryGirl.create(:user, name: Faker::Name.name, email: Faker::Internet.email, password: Faker::Internet.password, confirmed_at: Time.now) }
  let(:user2) { FactoryGirl.create(:user, name: Faker::Name.name, email: Faker::Internet.email, password: Faker::Internet.password, confirmed_at: Time.now) }

  before do
    @article = Article.create(title: "The first article", body: "Body of first article", user: user1)
  end

  scenario "Non signin user does not see edit or delete links" do
    visit '/articles'
    click_link @article.title

    expect(page).to have_content(@article.title)
    expect(page).to have_content(@article.body)
    expect(page.current_path).to eq(article_path(@article))
    expect(page).not_to have_link("Edit Article")
    expect(page).not_to have_link("Destroy Article")
  end

  scenario "A non-owner signed in cannot see both links" do
    login_as(user2)
    visit '/articles'

    click_link @article.title

    expect(page).not_to have_link("Edit Article")
    expect(page).not_to have_link("Destroy Article")
  end

  scenario "Display individual article" do
    visit '/articles'
    click_link @article.title
    
    expect(page).to have_content(@article.title)
    expect(page).to have_content(@article.body)
    expect(page.current_path).to eq(article_path(@article))
  end

  scenario "A signed owner sees both links" do
    login_as(user1)
    visit '/articles'

    click_link @article.title

    expect(page).to have_link("Edit Article")
    expect(page).to have_link("Destroy Article")
  end

end