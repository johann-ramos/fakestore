require 'rails_helper'

RSpec.feature "Creating Article" do
  let(:user) { FactoryGirl.create(:user)}
  before do
    login_as(user)
  end

  scenario "A user creates a new article" do
    visit "/articles"
    click_link "New Article"
   
    fill_in "Title", with: "Creating first article"
    fill_in "Body", with: "Lorem Ipsum"
    click_button "Create Article"

    expect(page).to have_content("Article was successfully created.")
    expect(page.current_path).to eq(articles_path)
    expect(page).to have_content("Created by: #{user.email}")
  end

  scenario "User fails to create a new article" do
    visit "/articles"
    click_link "New Article"
    
    fill_in "Title", with: ""
    fill_in "Body", with: ""
    click_button "Create Article"

    expect(page).to have_content("errors prohibited this article from being saved")
    expect(page).to have_content("Title can't be blank")
    expect(page).to have_content("Body can't be blank")
  end
end