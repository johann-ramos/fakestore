require 'rails_helper'

RSpec.feature "Creating Article" do

  before do
    @user1 = User.create(email: Faker::Internet.email, password: Faker::Internet.password)
    @user2 = User.create(email: Faker::Internet.email, password: Faker::Internet.password)
    @user1.confirm
    @user2.confirm
    @article = Article.create(title: "The first article", body: "Body of first article", user: @user1)
  end

  scenario "permits a signed in user to write a review" do
    login_as(@user1)
    visit '/articles'
    click_link @article.title
    fill_in 'comment_body', with: "An awesome article"
    click_button "Add comment"

    expect(page).to have_content("Comment was successfully created")
    expect(page).to have_content("An awesome article")
    expect(current_path).to eq(article_path(@article.id))
  end

end