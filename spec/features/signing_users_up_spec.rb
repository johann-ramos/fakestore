require 'rails_helper'

RSpec.feature "Users signin" do
  let(:user) { FactoryGirl.create(:user)}

  scenario "Login" do
    visit '/'
    click_link 'Log In'
    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_button 'Login'
    expect(page).to have_content("Signed in successfully.")
  end

end