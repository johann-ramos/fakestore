require 'rails_helper'
require 'support/macros'

RSpec.describe ArticlesController, type: :controller do

  describe "Get #edit" do
    before do
      @user1 = User.create(email: Faker::Internet.email, password: Faker::Internet.password)
      @user2 = User.create(email: Faker::Internet.email, password: Faker::Internet.password)
      @user1.confirm
      @user2.confirm
      @article = Article.create(title: "The first article", body: "Body of first article", user: @user1)
    end

    context "owner is allowed to edit his articles" do
      it "renders the edit template" do
        login_user(@user1)

        get :edit, id: @article
        expect(response).to render_template :edit
      end
    end

    context 'non-owner is not allowed to edit other users ariticles' do
      it 'redirects to the root path' do
        login_user(@user2)

        get :edit, id: @article
        expect(response).to redirect_to(root_path)
      end  
    end

  end

end