require 'rails_helper'

RSpec.describe CommentsController, type: :controller do

  before do
    @user = FactoryGirl.create(:user)
    @article = FactoryGirl.create(:article)
    @comment = FactoryGirl.create(:comment)
  end

  describe '#POST create' do
    context 'signed in user with valid attributes' do
      it 'creates a new comment' do
        sign_in(@user)
        post :create, comment: { :body => @comment.body }, :article_id => @article.id
        expect(Comment.last.body).to eq(@comment.body)
        expect(flash[:notice]).to eq("Comment was successfully created.")
      end
    end
    context 'non signed-in users' do
      it 'is redirected to the sign in page' do
        post :create, comment: { :body => @comment.body }, :article_id => @article.id
        expect(response).to redirect_to new_user_session_path
      end
    end
    context 'with invalid attributes' do
      it 'does not save the new contact in the database' do
        sign_in(@user)
        post :create, comment: { :body => '' }, :article_id => @article.id

        expect(response).to render_template(:new)
      end
    end
  end
end
