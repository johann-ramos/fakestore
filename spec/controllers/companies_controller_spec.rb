require 'rails_helper'

RSpec.describe CompaniesController, type: :controller do

  before do
    @user = FactoryGirl.create(:user)
    sign_in(@user)
  end

  describe '#POST company' do
    context 'with valid data' do
      it 'creates a new company' do
        post :create, company: { :name => "Yupi", :manager => "johann", :status => "active", :terms => 3}
        expect(Company.last.name).to eq("Yupi")
      end
    end
  end

end

  # Parameters: {"utf8"=>"✓", "authenticity_token"=>"MGH3w7qUCV1AqLI1n+z97pbKaQbBelUHcWYo9sXA9B2Unbz77Chw8AV9n588i63ijzVnvXSag7qKLOmy3htMwg==", "company"=>{"name"=>"Basement", "manager"=>"johann ramos", "status"=>"active", "terms"=>"13"}, "commit"=>"Create Company"}