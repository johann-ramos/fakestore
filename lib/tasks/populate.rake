namespace :db do
  desc "Erase and fill database"
  task :populate => :environment do
    require 'populator'

      [Company, Listing, Article, Invoice].each(&:delete_all)

      Company.populate 20 do |company|
        company.name = Faker::Company.name
        company.manager = Faker::Name.name
        company.status = ["active","inactive"]
        company.terms = rand(1..5)
        company.created_at = Faker::Date.between(2.days.ago, Date.today)
        company.updated_at = company.created_at
      end

      Listing.populate 50 do |listing|
        listing.name = ["Ubuntu", "Debian", "Fedora", "Archlinux", "Suse", "Mouse", "Keyboard"]
        listing.description = Faker::Lorem.sentences
        listing.price = Faker::Commerce.price
        listing.created_at = Faker::Date.between(2.days.ago, Date.today)
        listing.updated_at = listing.created_at
        listing.user_id = User.ids
      end

      Article.populate 25 do |article|
        article.title = Faker::Lorem.sentence
        article.body = Faker::Lorem.paragraph(2)
        article.created_at = Faker::Date.between(2.days.ago, Date.today)
        article.updated_at = article.created_at
        article.user_id = User.ids
      end

      Invoice.populate 25 do |invoice|
        invoice.company = Faker::Company.name
        invoice.tax = Faker::Commerce.price
        invoice.salesperson = Faker::Name.name
        invoice.created_at = Faker::Date.between(2.months.ago, Date.today)
        invoice.updated_at = invoice.created_at
      end
  end
end